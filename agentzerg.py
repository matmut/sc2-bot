# -*- coding: utf-8 -*-
"""
Created on Mon Mar 16 11:54:57 2020

@author: Aurelien
"""

"""
TODO

actions :
- créer un ouvrier
- créer un batiment (en paramètre le type de batiment)
    - selectionne un drone
    - dire ou placer le batiment (ou c'est le problème le plus compliqué, placer un batiment)
- attaquer
- créer des overlord

- réussir à se servir de la minimap

"""

import numpy
import random
from pysc2.agents import base_agent
from pysc2.env import sc2_env
from pysc2.lib import actions, features, units
from absl import app

_NO_OP = actions.FUNCTIONS.no_op.id
_PLAYER_RELATIVE = features.SCREEN_FEATURES.player_relative.index

class Base(base_agent.BaseAgent):
    b = False
    c= False
    def __init__(self):
        self.etape=0
        self.action=0
        self.reward = 0
        self.episodes = 0
        self.steps = 0
        self.obs_spec = None
        self.action_spec = None
        self.minerals_nb = 0
        self.gas = 0
        self.population = 0
        self.population_max = 0
        self.population_armee = 0
        self.population_worker = 0
        self.spawning_pools=0
        self.attack_time = 0
        self.obs=None
    
    #renvoie un tableau d'unité du type demandé
    def get_units_by_type(self, obs, unit_type):
         return [unit for unit in obs.observation.feature_units if unit.unit_type == unit_type if unit.alliance == features.PlayerRelative.SELF]

    def get_units_in_build_queue(self, obs, unit_type):
         return [unit for unit in obs.observation.build_queue if unit.unit_type == unit_type if unit.player_relative == features.PlayerRelative.SELF]

    def get_enemy(self):
        position = self.get_all_enemy()
        unit_id = int(random.random() * len(position))
        return [position[unit_id]['x'], position[unit_id]['y']]

    def get_all_enemy(self):
        position = []
        for y in range(self.obs.observation.feature_minimap.shape[2]):
            for x in range(self.obs.observation.feature_minimap.shape[1]):
                if self.obs.observation.feature_minimap.player_relative[y,x] == features.PlayerRelative.ENEMY:
                    position.append({'x': x, 'y': y, 'unit_type': self.obs.observation.feature_minimap.unit_type[y, x]})
        return position
    
    #renvoie si on a selectionner une unité de ce type
    def unit_type_is_selected(self, obs, unit_type):
        print(obs.observation.single_select)
        if (len(obs.observation.single_select) > 0 and
            obs.observation.single_select[0].unit_type == unit_type):
            return True
        print(obs.observation.multi_select)
        if (len(obs.observation.multi_select) > 0 and
            obs.observation.multi_select[0].unit_type == unit_type):
            return True
    
        return False
    def _select_random_drone(self):
        print("drone")
        drones = self.get_units_by_type(self.obs, units.Zerg.Drone)
        if len(drones) == 0:
            raise Exception("No drones are available to select")
        drone = random.choice(drones)
        if drone.x <0:
            drone.x *=-1
        if drone.y <0:
            drone.y *=-1
        return actions.FUNCTIONS.select_point("select", [drone.x, drone.y])

    def _create_spawning_pool(self):
        if self.etape==0:
            self.etape=1
            return self._select_random_drone()
        elif actions.FUNCTIONS.Build_SpawningPool_screen.id in self.obs.observation.available_actions:
            print('pool')
            units = [unit for unit in self.obs.observation.feature_units if unit.alliance == features.PlayerRelative.SELF]
            unit_id = int(random.random() * len(units))
            unit_xy = [units[unit_id].x, units[unit_id].y]
            self.etape=0
            return actions.FUNCTIONS.Build_SpawningPool_screen("now", unit_xy)
        else :
            return self._select_random_drone()
        return actions.FunctionCall(actions.FUNCTIONS.no_op.id, [])


    def _create_zerglings(self):
        larvas = self.get_units_by_type(self.obs, units.Zerg.Larva)
        if len(larvas) > 0 and self.etape==0:
            larva = random.choice(larvas)
            self.etape=1
            return actions.FUNCTIONS.select_point("select_all_type", (larva.x,larva.y))
        if self.etape==1:
            self.etape=0
            if (actions.FUNCTIONS.Train_Zergling_quick.id in self.obs.observation.available_actions):
                return actions.FUNCTIONS.Train_Zergling_quick("now")
        return actions.FunctionCall(actions.FUNCTIONS.no_op.id, [])


    def _create_drones(self):
        larvas = self.get_units_by_type(self.obs, units.Zerg.Larva)
        if len(larvas) > 0 and self.etape==0:
            larva = random.choice(larvas)
            self.etape=1
            return actions.FUNCTIONS.select_point("select_all_type", (larva.x,larva.y))
        if self.etape==1:
            self.etape=0
            if (actions.FUNCTIONS.Train_Drone_quick.id in self.obs.observation.available_actions):
                return actions.FUNCTIONS.Train_Drone_quick("now")
        return actions.FunctionCall(actions.FUNCTIONS.no_op.id, [])

    def _create_overlord(self):
        larvas = self.get_units_by_type(self.obs, units.Zerg.Larva)
        if len(larvas) > 0 and self.etape==0:
            larva = random.choice(larvas)
            self.etape=1
            return actions.FUNCTIONS.select_point("select_all_type", (larva.x,larva.y))
        if self.etape==1:
            self.etape=0
            if (actions.FUNCTIONS.Train_Overlord_quick.id in self.obs.observation.available_actions):
                return actions.FUNCTIONS.Train_Overlord_quick("now")
        return actions.FunctionCall(actions.FUNCTIONS.no_op.id, [])

    def _create_queen(self):
        hatchery = self.get_units_by_type(self.obs, units.Zerg.Hatchery)
        if self.etape==0:
            self.etape=1
            return actions.FUNCTIONS.select_point("select_all_type", (hatchery[0].x,hatchery[0].y))
        if self.etape==1:
            self.etape=0
            if (actions.FUNCTIONS.Train_Queen_quick.id in self.obs.observation.available_actions):
                return actions.FUNCTIONS.Train_Queen_quick("now")
        return actions.FunctionCall(actions.FUNCTIONS.no_op.id, [])

    def selec_action(self):
        print(self.minerals_nb)
        if self.attack_time > 0:
            self.attack_time -= 1
        if self.population_armee > 5 and self.attack_time==0:
            self.attack_time = 10
            return 4
        if self.minerals_nb >=50 :
            self.spawning_pools = self.get_units_by_type(self.obs, units.Zerg.SpawningPool)
            self.spawning_pools.extend(self.get_units_in_build_queue(self.obs, units.Zerg.SpawningPool))
            print("nb"+str(len(self.spawning_pools)))
            if self.population_worker>=12 and len(self.spawning_pools) == 0:
                return 0
            if self.population>=self.population_max and len(self.get_units_in_build_queue(self.obs, units.Zerg.Overlord)) == 0 and len(self.spawning_pools) > 0:
                return 2
            if len(self.spawning_pools) == 1:
                larvas = self.get_units_by_type(self.obs, units.Zerg.Larva)
                if len(larvas)>0:
                   return 3
                else:
                    return 5

            if self.population_worker<=13:
                return 1
        return -1

    def attack(self):
        if self.population_armee > 0 and self.etape==0:
            if (actions.FUNCTIONS.select_army.id in self.obs.observation.available_actions):
                self.etape=1
                return actions.FUNCTIONS.select_army(True, False)
        if self.etape==1:
            if (actions.FUNCTIONS.Attack_minimap.id in self.obs.observation.available_actions):
                self.etape=0
                position = self.get_enemy()
                return actions.FUNCTIONS.Attack_minimap('now', position)
        return actions.FunctionCall(actions.FUNCTIONS.no_op.id, [])

    def play(self,):
        #choisie une action
        if self.etape==0:
            self.action=self.selec_action()
            print(self.action)

        if self.action==0:
            return self._create_spawning_pool()
        elif self.action==1:
            return self._create_drones()
        elif self.action==2:
            return self._create_overlord()
        elif self.action==3:
            return self._create_zerglings()
        elif self.action==4:
            return self.attack()
        elif self.action==5:
            return self._create_queen()
        elif self.action==-1:
            return actions.FunctionCall(actions.FUNCTIONS.no_op.id, [])


    def step(self, obs):
        super(Base, self).step(obs)

        """
        obs.observation
        dict_keys(['single_select', 'multi_select', 'build_queue', 'cargo', 'production_queue', 'last_actions', 'cargo_slots_available', 'home_race_requested', 'away_race_requested', 'map_name', 'feature_screen', 'feature_minimap', 'action_result', 'alerts', 'game_loop', 'score_cumulative', 'score_by_category', 'score_by_vital', 'player', 'control_groups', 'upgrades', 'available_actions'])
        """
        print(",n")
        #print(obs.observation.player)
        #print(obs)
        self.obs=obs
        self.minerals_nb = obs.observation.player[1]
        self.gas = obs.observation.player[2]
        self.population = obs.observation.player[3]
        self.population_max = obs.observation.player[4]
        self.population_armee = obs.observation.player[5]
        self.population_worker = obs.observation.player[6]
        marines=" "

        """""
        self.spawning_pools = self.get_units_by_type(obs, units.Zerg.SpawningPool)
        if len(self.spawning_pools) == 0:
            if self.unit_type_is_selected(obs, units.Zerg.Drone):
                if (actions.FUNCTIONS.Build_SpawningPool_screen.id in obs.observation.available_actions):
                    x = random.randint(0, 32)
                    y = random.randint(0, 32)
                    print(x,y)
                    return actions.FUNCTIONS.Build_SpawningPool_screen("now", [ x, y])
        
        
        
        
        
            drones = [unit for unit in obs.observation.feature_units if unit.unit_type == units.Zerg.Drone]
            return actions.FUNCTIONS.select_point("select", [drones[0].x,drones[0].y])
        else :
            if self.unit_type_is_selected(obs, units.Zerg.Larva):
              if (actions.FUNCTIONS.Train_Zergling_quick.id in obs.observation.available_actions):
                return actions.FUNCTIONS.Train_Zergling_quick("now")
              if (actions.FUNCTIONS.Train_Overlord_quick.id in obs.observation.available_actions):
                  return actions.FUNCTIONS.Train_Overlord_quick("now")
            larvas = self.get_units_by_type(obs, units.Zerg.Larva)
            if len(larvas) > 0:
              larva = random.choice(larvas)
              return actions.FUNCTIONS.select_point("select_all_type", (larva.x,larva.y))
        """""

        
        
        
        
        
        
        
        
        
        
        
        
        
        """
        for unit in obs.observation.feature_units:
            if unit.alliance == features.PlayerRelative.SELF:
                print(unit) 
        """
        
        """
        if not self.b:
            print("select")
            drones = [unit for unit in obs.observation.feature_units
              if unit.unit_type == units.Zerg.Drone]
            self.b = True
            return actions.FUNCTIONS.select_point("select", drones[0].x,drones[1].y)
        else:
            if self.c:
                return actions.FunctionCall(actions.FUNCTIONS.Train_Drone_quick, [[0]])
                
            else :
                print("train")
                print(obs.observation.available_actions)
                self.c=True
                return actions.FunctionCall(actions.FUNCTIONS.select_larva, [[0]])
            
        print(marines)
        print(len(marines))
        """
 
        """
        if population < population_max and actions.FUNCTIONS.Train_Probe_quick.id in obs.observation:
            print("increase pop")
            return actions.FunctionCall(actions.FUNCTIONS.Train_Probe_quick.id, [])
        
        else:
            print("random")
            """
        return self.play()
        #print(obs.observation.available_actions.keys())


def main(unused_argv):
    agent = Base()
    try:
        while True:
            with sc2_env.SC2Env(
                    map_name="Flat64",
                    players=[sc2_env.Agent(sc2_env.Race.zerg),
                             sc2_env.Bot(sc2_env.Race.zerg,
                                         sc2_env.Difficulty.very_easy,
                                         [sc2_env.BotBuild.timing])],
                    agent_interface_format=features.AgentInterfaceFormat(
                        feature_dimensions=features.Dimensions(screen=84, minimap=64),
                        use_feature_units=True,
                        allow_cheating_layers=True),
                    step_mul=16,
                    game_steps_per_episode=0,
                    visualize=True,
                    disable_fog=True) as env:

                agent.setup(env.observation_spec(), env.action_spec())

                timesteps = env.reset()
                agent.reset()
                obs = env.step(actions=[actions.FunctionCall(_NO_OP, [])])
                player_relative = obs[0].observation["feature_screen"][_PLAYER_RELATIVE]

                # Break Point!!
                print(player_relative)

                while True:
                    step_actions = [agent.step(timesteps[0])]
                    if timesteps[0].last():
                        break
                    timesteps = env.step(step_actions)
                break

    except KeyboardInterrupt:
        pass


if __name__ == "__main__":
    app.run(main)
