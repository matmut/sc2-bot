# Architecture en réseaux de neurones de l'agent

## Réseau principal

L'agent commence par utiliser un **réseau principal** qui prendra des **décisions grossières**. Une fois cette décision prise, c'est le **sous-réseau correspondant** qui prend le relai jusqu'à la fin de l'action.   
Pendant qu'un sous-réseau gère une action en cours, le réseau principal peut continuer de fonctionner pour lancer de **nouvelles actions en parallèle**.

**Entrées** :
- Population totale / population max
- Nombre de zerglings
- Réserve minerais
- Nombre de péons
- Population armée adverse

**Sorties** :
- Attaquer
- Produire zergling
- Produire peon

## Sous-réseaux
### Attaquer

Ce sous-réseau doit gérer une **attaque en cours**, c'est-à-dire gérer des troupes qui ont été envoyées depuis la base de l'agent vers des positions adverses, pour les attaquer.

**Entrées** :
- Nombre de zerglings présents dans l'attaque / Population armée adverse
- Nombre de zerglings disponibles à la base
- Population armée adverse en train d'attaquer la base de l'agent

**Sorties** :
- Cibler la base ennemie
- Cibler les troupes ennemies
- Appeler des renforts
- Rappeler les zerglings à la base

### Produire zergling

Il n'y aura pas forcément besoin d'un sous-réseau pour gérer cette action.

### Produire péon

Il n'y aura pas forcément besoin d'un sous-réseau pour gérer cette action.