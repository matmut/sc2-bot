from pysc2.lib import actions, features, units
import random


class AbstractPlayer:
    def __init__(self, agent, obs):
        self.agent = agent
        self.obs = obs


    # https://github.com/deepmind/pysc2/blob/master/pysc2/lib/features.py
    def _get_player_stats(self):
        return {
            "player_id": self.obs.observation.player[0],
            "minerals": self.obs.observation.player[1],
            "vespene": self.obs.observation.player[2],
            "food_used": self.obs.observation.player[3],
            "food_cap": self.obs.observation.player[4],
            "food_army": self.obs.observation.player[5],
            "food_workers": self.obs.observation.player[6],
            "idle_worker_count": self.obs.observation.player[7],
            "army_count": self.obs.observation.player[8],
            "warp_gate_count": self.obs.observation.player[9],
            "larva_count": self.obs.observation.player[10]
        }

    # https://github.com/deepmind/pysc2/blob/master/docs/environment.md#minimap
    # https://github.com/deepmind/pysc2/blob/master/pysc2/lib/renderer_ascii.py
    def show_minimap(self):
        for y in range(self.obs.observation.feature_minimap.shape[2]):
            for x in range(self.obs.observation.feature_minimap.shape[1]):
                if self.obs.observation.feature_minimap.visibility_map[y,x] == 1 and self.obs.observation.feature_minimap.player_relative[y,x] < 3:
                    print("B ", end='')
                else:
                    print("x ", end='')
            print("")

        print("\n")

    def get_units_by_type(self, obs, unit_type):
        return [unit for unit in obs.observation.feature_units if unit.unit_type == unit_type]

    def get_units_in_build_queue(self, obs, unit_type):
         return [unit for unit in obs.observation.build_queue if unit.unit_type == unit_type if unit.player_relative == features.PlayerRelative.SELF]

    def get_enemy(self):
        position = []
        for y in range(self.obs.observation.feature_minimap.shape[2]):
            for x in range(self.obs.observation.feature_minimap.shape[1]):
                if self.obs.observation.feature_minimap.player_relative[y, x] == features.PlayerRelative.ENEMY:
                    position.append([x, y])
        unit_id = int(random.random() * len(position))
        return position[unit_id]

    def _spawning_pool_exists(self):
        return len(self.agent.get_units_by_type(self.obs, units.Zerg.SpawningPool)) > 0

    def _get_drone_list(self):
        return [unit for unit in self.obs.observation.feature_units if unit.unit_type == units.Zerg.Drone]

    # The Drone is the basic worker unit for Zerg.
    # It can harvest Minerals and Vespene Gas as well as build any Zerg structure at the cost of its life.
    def _select_random_drone(self):
        print("drone")
        drones = self._get_drone_list()
        if len(drones) == 0:
            raise Exception("No drones are available to select")
        drone = random.choice(drones)
        return actions.FUNCTIONS.select_point("select", [drone.x, drone.y])

    # The Spawning Pool is the first Zerg tech building; it unlocks Zerg's basic fighting units - Zerglings.
    # Building the Spawning pool also unlocks the Lair, Roach Warren, Baneling Nest, Spine Crawler,
    # Spore Crawler and the Queen.
    def _create_spawning_pool(self):
        if self.agent.etape==0:
            self.agent.etape=1
            return self._select_random_drone()
        elif actions.FUNCTIONS.Build_SpawningPool_screen.id in self.obs.observation.available_actions:
            print('pool')
            units = [unit for unit in self.obs.observation.feature_units if unit.alliance == features.PlayerRelative.SELF]
            unit_id = int(random.random() * len(units))
            unit_xy = [units[unit_id].x, units[unit_id].y]
            self.agent.etape=2
            return actions.FUNCTIONS.Build_SpawningPool_screen("now", unit_xy)
        return actions.FunctionCall(actions.FUNCTIONS.no_op.id, [])

    def _create_zerglings(self):
        larvas = self.get_units_by_type(self.obs, units.Zerg.Larva)
        if len(larvas) > 0 and self.agent.etape==0:
            larva = random.choice(larvas)
            return actions.FUNCTIONS.select_point("select_all_type", (larva.x,larva.y))
        if self.agent.etape==1:
            if (actions.FUNCTIONS.Train_Zergling_quick.id in self.obs.observation.available_actions):
                return actions.FUNCTIONS.Train_Zergling_quick("now")
        return actions.FunctionCall(actions.FUNCTIONS.no_op.id, [])


    def build_structure(self, type):
        pass

    def attack(self):
        if self.agent.population_armee > 0 and self.agent.etape==0:
            if (actions.FUNCTIONS.select_army.id in self.obs.observation.available_actions):
                self.etape=1
                return actions.FUNCTIONS.select_army(True, False)
        if self.agent.etape==1:
            if (actions.FUNCTIONS.Attack_minimap.id in self.obs.observation.available_actions):
                self.agent.etape=0
                position = self.get_enemy()
                return actions.FUNCTIONS.Attack_minimap('now', position)
        return actions.FunctionCall(actions.FUNCTIONS.no_op.id, [])

    def play(self):
        #continue l'action non finie
        if self.agent.etape>0:
            return self._create_spawning_pool()
        #choisie une action
        else:
            return self._create_spawning_pool()
